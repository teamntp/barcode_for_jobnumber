# README #

Application for reading data from barcode scanner connected to raspberry pi.

### What the application does? ###

Takes the data from serial device and stores it in a variable

### Structure of an application? ###

* Contains only MainPage.cs

* Namespaces used in addition to the default ones
	- using Windows.Devices.Enumeration;
	- using Windows.Devices.SerialCommunication;
	- using Windows.Storage.Streams;
* Methods in use
	- scanDevice() : Look for connected usb devices. Generates new cancellation token for later use.
	- OpenPort() : Connect to serial device with mentioned settings like Baudrate, databits etc.
	- Listen() : creates datareader object to take input.
	- ReadAsync() : reads data from serial device and store it inside variable (barcodeData). Also same data is stored in long integer variable (jobNumber).
	- CancelReadTask() : This activates cancellation token which was previously generated. This method is called when app exits/page_unloaded.
	
Note : jobNumber is the data that is to be sent to API along with existing data of sheet count and machine ID.


### Hardware Configuration : ###

Barcode scanner : rtscan RT235
USB Interface : USB CDC (serial communication)
If this scanner is connected to pc then it appears under serial port section in device manager.

key for "..._TemporaryKey.pfx_" : ntp1234

### Who do I talk to? ###

* Juan Pablo
* Sindre
* Shailesh